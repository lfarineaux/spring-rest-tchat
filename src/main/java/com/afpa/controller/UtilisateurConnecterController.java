package com.afpa.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dao.IUtilisateurDao;

@RestController
public class UtilisateurConnecterController {

	@Autowired
	IUtilisateurDao monDao;
	
	
	@GetMapping("/connecter")
	public Authentication test(Authentication authentication){
		System.err.println(authentication.getName());
		return authentication;
		
	}
	@GetMapping("/deconnexion")
	public void controleurDeconnection(HttpServletRequest request,HttpServletResponse res) throws IOException {
		request.getSession().invalidate();
		res.sendRedirect("/");
	}
	
	@GetMapping("/")
	public ModelAndView connexion(ModelAndView mv) {
		mv.setViewName("login.html");
		return mv;
		
	}
}
