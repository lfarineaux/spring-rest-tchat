package com.afpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.UtilisateurDto;
import com.afpa.service.IUtilisateurService;

@RestController
//@RequestMapping("/utilisateur")
public class UtilisateurControleur {

	@Autowired
	IUtilisateurService utilisateurServ;
	
	@GetMapping(path = "/users")
	public List<UtilisateurDto>pageListUtilisateur() {
		return this.utilisateurServ.maListeUtilisateur();
	}
	
	@PostMapping("/adduser")
	public void pageAjoutUtilisateur(@RequestBody UtilisateurDto u,BCryptPasswordEncoder bcryp) {
		this.utilisateurServ.ajoutUtilisateur(u, bcryp);
	
	}
	
	@GetMapping(path = {"/login"})
	public ModelAndView coucou(ModelAndView mv) {
		mv.addObject("msg", "Linda");
		mv.setViewName("login");
		return mv;
	}
	
}
