package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Message;

@Repository
public interface MessageDao extends CrudRepository<Message, Integer>{

	public List<Message> findAll();
}
