package com.afpa.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Utilisateur;

@Repository
public interface IUtilisateurDao extends CrudRepository<Utilisateur, Integer>{

	public List<Utilisateur> findAll();

	public Optional<Utilisateur> findUtilisateurByUsername(String username);
	
}
