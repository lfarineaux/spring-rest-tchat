package com.afpa.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afpa.dao.IUtilisateurDao;
import com.afpa.entity.Utilisateur;

@Service
public class CdaUserDetailsService implements UserDetailsService {
 
    @Autowired
    private final IUtilisateurDao userDao;
    
    public CdaUserDetailsService(IUtilisateurDao userDao) {
        this.userDao = userDao;
    }
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        Utilisateur user = userDao.findUtilisateurByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new CdaUserPrincipal(user);
    }
}
