package com.afpa.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.afpa.entity.Utilisateur;


public class CdaUserPrincipal implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	private Utilisateur user;
	private List<GrantedAuthority> authorities;
	 
    public CdaUserPrincipal(Utilisateur user) {
    	this.user = user;
    	this.authorities = new ArrayList<>();
    	this.authorities.add(new SimpleGrantedAuthority(user.getUsername()));
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}
	
	public String getUserId() {
		
		return ""+this.user.getId();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
