package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IUtilisateurDao;
import com.afpa.dao.MessageDao;
import com.afpa.dto.MessageDto;
import com.afpa.dto.UtilisateurDto;
import com.afpa.entity.Message;
import com.afpa.entity.Utilisateur;
@Service
public class MessageService implements IMessageService{

	@Autowired
	MessageDao messageDao;
	
	@Autowired
	private IUtilisateurDao utilisateurDao;
	
	
	@Override
	public MessageDto findById(int id) {
		Optional<Message> msg = this.messageDao.findById(id);
		MessageDto messageDto = null;
		if (msg.isPresent()) {
			Message message = msg.get();
			messageDto = new MessageDto().builder().id(message.getId()).message(message.getMessage()).build();
		}
		return messageDto;
	}

	@Override
	public void ajouterMessage( MessageDto build) {
		Utilisateur u = this.utilisateurDao.findById(build.getUtilisateur().getId()).get();
		Message m = Message.builder().
		message(build.getMessage())
		.utilisateur(u)
		.build();
		this.messageDao.save(m);
	}

	@Override
	public List<MessageDto> findAllMessage() {
		
		return this.messageDao.findAll().stream().map(message->MessageDto
				.builder()
				.message(message.getMessage())
				.utilisateur(UtilisateurDto.builder().username(message.getUtilisateur().getUsername()).build())
				.build())
				.collect(Collectors.toList());

	}

	@Override
	public void updateMessage(int id, String message) {
		Optional<Message> msg = this.messageDao.findById(id);
		if (msg.isPresent()) {
			Message m = msg.get();
			m.setMessage(message);
			this.messageDao.save(m);
		}
		
	}

}
