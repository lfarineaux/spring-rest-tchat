package com.afpa.service;

import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.afpa.dto.UtilisateurDto;

public interface IUtilisateurService {

	public List<UtilisateurDto> maListeUtilisateur();
	
	public UtilisateurDto afficherUnUtilisateur(int id);
	
	public void supprimerUtilisateur(int id);
	
	public void majUtilisateur(int id, String username, String password);
	
	public void ajoutUtilisateur(UtilisateurDto u,BCryptPasswordEncoder bcryp);
	
}
