package com.afpa.service;

import java.util.List;

import com.afpa.dto.MessageDto;

public interface IMessageService {
	
	public MessageDto findById(int id);
	
	public void ajouterMessage( MessageDto build);
	
	List<MessageDto> findAllMessage();
	
	void updateMessage(int id, String message);

}