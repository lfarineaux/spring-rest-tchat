package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.afpa.dao.IUtilisateurDao;
import com.afpa.dto.UtilisateurDto;
import com.afpa.entity.Utilisateur;

@Service
public class UtilisateurServiceImp implements IUtilisateurService{

	@Autowired
	private IUtilisateurDao utilisateurDao;
	
	
	@Override
	public List<UtilisateurDto> maListeUtilisateur() {
		List<UtilisateurDto> maliste = this.utilisateurDao.findAll().stream()
				.map(u -> UtilisateurDto.builder()
				.username(u.getUsername())
				.id(u.getId())
				.password(u.getPassword())
				.build())
				.collect(Collectors.toList());
		
		return maliste;
	}

	@Override
	public UtilisateurDto afficherUnUtilisateur(int id) {
		Optional<Utilisateur> monUtilisateur = this.utilisateurDao.findById(id);
		
		return UtilisateurDto.builder()
				.id(monUtilisateur.get().getId())
				.username(monUtilisateur.get().getUsername())
				.password(monUtilisateur.get().getPassword())
				.build();
	}

	@Override
	public void supprimerUtilisateur(int id) {
		
		Optional<Utilisateur> monUtilisateur = this.utilisateurDao.findById(id);
		this.utilisateurDao.delete(monUtilisateur.get());
		
	}

	@Override
	public void majUtilisateur(int id, String username, String password) {
		Optional<Utilisateur> monUtilisateur = this.utilisateurDao.findById(id);
		if(monUtilisateur.isPresent()) {
			this.utilisateurDao.save(monUtilisateur.get().builder()
					.username(username)
					.password(password)
					.build());
		}
	}

	@Override
	public void ajoutUtilisateur(UtilisateurDto u, BCryptPasswordEncoder bcryp) {
		this.utilisateurDao.save(Utilisateur.builder()
				.username(u.getUsername())
				.password(bcryp.encode(u.getPassword()))
				.build());
	}

}
