package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "msg_seq")
	private int id;
	private String message;

	@ManyToOne
	private Utilisateur utilisateur;
}
