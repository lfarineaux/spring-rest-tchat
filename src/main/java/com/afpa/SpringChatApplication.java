package com.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class SpringChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringChatApplication.class, args);
	}

//	@Bean
//	public ModelMapper getModelMapper() {
//		return new ModelMapper();
//	}
//	
//	@Bean
//    public CommandLineRunner init (IUtilisateurDao utilisateurDao, BCryptPasswordEncoder bcryp){
//        return args -> {
//        	
//            Utilisateur userAdmin = null;	
//        	
//        	String adminPassword = RandomStringUtils.randomAlphabetic(6);
//            	StringBuilder sb = new StringBuilder();
//            	sb.append("\n\n*****\n\n")
//            		.append("insertion compte admin ...")
//            		.append("\n")
//            		.append("mot de passe : "+adminPassword)
//            		.append("\n")
//            		.append("\n\n*****\n");
//            	log.error(sb.toString());
//        	
//            	userAdmin = utilisateurDao.save(Utilisateur.builder()
//            			.username("admin")
//            			.password(bcryp.encode(adminPassword))
//            			.build());
//            	
//        };
//    }
//	
//	
}
